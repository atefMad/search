import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TranslationService extends Translations {
  static const locale = Locale('en', 'US');
  static const fallbackLocale = Locale('en', 'US');
  late SharedPreferences pref;
  String language = 'english';

  static final langs = [
    'English',
    'Arabic',
  ];

  static const locales = [
    Locale('en', 'US'),
    Locale('ar', 'SA'),
  ];
  @override
  Map<String, Map<String, String>> get keys => {
        'en': {'resturant1': 'KFC', "location": "Lattakia"},
        'ar': {'resturant1': 'كي إف سي', "location": "اللاذقية"},
      };

  void changeLocale(String lang) async {
    pref = await SharedPreferences.getInstance();
    final localeIndex = langs.indexOf(lang);
    if (localeIndex != -1) {
      Get.updateLocale(locales[localeIndex]);
      pref.setString('lang', lang);
    }
  }

  void initPref() async {
    pref = await SharedPreferences.getInstance();
  }

  bool isArabic() {
    // final lang = pref.getString('lang');
    // if (language != null) {
      return language == 'arabic' ? true : false;
    // }
    // return true;
  }
}
