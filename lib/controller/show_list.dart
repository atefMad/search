import 'package:get/get_state_manager/src/simple/get_controllers.dart';

class ShowListController extends GetxController{

   bool isGrid = true;
  
  void changeShow(bool b){
    isGrid = b;
    update();
  }
}