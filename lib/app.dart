import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:newwork/view/screens/search_screen.dart';

import 'controller/translation.dart';

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final TranslationService localizationService =
        Get.put(TranslationService());
    return ScreenUtilInit(
        designSize: const Size(540, 1000),
        builder: (BuildContext context, Widget? child) {
          return GetMaterialApp(
            translations: TranslationService(),
            locale: TranslationService.locale,
            fallbackLocale: TranslationService.fallbackLocale,
            debugShowCheckedModeBanner: false,
            home: const SearchScreen(),
          );
        });
  }
}
