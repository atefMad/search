
import 'package:flutter/material.dart';

const colorOne = Color(0xff287094);

const colorTwo = Color(0xff023246);

const colorThree = Color(0xff1E465A);

const colorFour = Color(0xffCED1D7);

const colorFive = Color(0xffE6E6E6);

const colorSix = Color(0xffFFCC33);

const colorSeven = Color(0xffED254E);