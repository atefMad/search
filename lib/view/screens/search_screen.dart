import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:newwork/config/theme/app_colors.dart';
import 'package:newwork/controller/show_list.dart';
import 'package:newwork/view/widgets/app_bar_widget.dart';
import 'package:newwork/view/widgets/grid_order.dart';
import 'package:newwork/view/widgets/list_order.dart';

class SearchScreen extends StatefulWidget {
  const SearchScreen({super.key});

  @override
  State<SearchScreen> createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const AppBarWidget(),
        toolbarHeight: 110.h, //sw  sh
        backgroundColor: colorThree,
      ),
      body: Container(
          height: double.infinity,
          decoration: const BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/images/BG.png'), fit: BoxFit.cover),
          ),
          child: GetBuilder<ShowListController>(
            init: ShowListController(),
            builder: (controller) {
              return controller.isGrid ?  GridOrder() : const ListOrder();
            },
          )),
    );
  }
}
