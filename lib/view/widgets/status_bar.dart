import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class StatusBar extends StatelessWidget {
  final Color color;
  final double height;
  final double width;
  const StatusBar({super.key, required this.color, required this.height, required this.width});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin:  EdgeInsets.symmetric(vertical: 2.h),
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.circular(5)
      ),
      height: height,
      width: width,
    );
  }
}