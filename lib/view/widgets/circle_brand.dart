import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:newwork/config/theme/app_colors.dart';

class CircleBrand extends StatelessWidget {
  final double distance;
  final String unit;
  const CircleBrand({super.key, required this.distance, required this.unit});

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(1000),
          color: colorSeven,
        ),
        height: 155.w,
        width: 155.w,
      ),
      Positioned.fill(
        child: Align(
          alignment: Alignment.topLeft,
          child: Container(
            decoration:
                const BoxDecoration(color: colorOne, shape: BoxShape.circle),
            alignment: Alignment.center,
            height: 40.w,
            width: 40.w,
            child: Column(
              children: [
                Text(
                  "$distance",
                  style: TextStyle(
                      color: colorFive,
                      fontFamily: 'SourceSansPro',
                      fontSize: 18.sp),
                ),
                Text(
                  unit,
                  style: TextStyle(
                      color: colorFive,
                      fontFamily: 'SourceSansPro',
                      fontSize: 11.sp,
                      fontWeight: FontWeight.w800),
                ),
              ],
            ),
          ),
        ),
      )
    ]);
  }
}
