import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:newwork/config/theme/app_colors.dart';
import 'package:newwork/view/widgets/location_text.dart';
import 'package:newwork/view/widgets/status_bar.dart';

class ListOrder extends StatelessWidget {
  const ListOrder({super.key});

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      padding: EdgeInsets.symmetric(horizontal: 12.w, vertical: 12.h),
      itemBuilder: (context, index) {
        return Row(
          children: [
            whiteSpace(),
            StatusBar(color: Colors.red[400]!, height: 30.h, width: 4.w),
            whiteSpace(),
            CircleAvatar(
              backgroundColor: colorSeven,
              radius: 15.w,
            ),
            whiteSpace(),
            SizedBox(
              width: 205.w,
              child: Text(
                  'resturant1'.tr,
                style: textStyle(
                    color: colorFive,
                    font: 'SourceSansPro',
                    weight: FontWeight.w800,
                    size: 18.sp),
              ),
            ),
            whiteSpace(),
            Image.asset(
              'assets/images/Asset 1@2x.png',
              width: 15.w,
            ),
            whiteSpace(),
            const LocationText(
              distance: 500.0,
              unit: 'Meter',
            ),
            whiteSpace(),
            SizedBox(
              width: 110.w,
              child: Text(
                "01111000111",
                style: textStyle(
                    font: 'SourceSansPro',
                    color: colorSix,
                    weight: FontWeight.normal,
                    size: 18.sp),
              ),
            ),
            whiteSpace(),
          ],
        );
      },
      separatorBuilder: (context, index) {
        return const Divider(
          color: colorFour,
        );
      },
      itemCount: 9,
      shrinkWrap: true,
    );
  }

  whiteSpace() => SizedBox(
        width: 6.w,
      );

  textStyle(
          {required String font,
          required Color color,
          required double size,
          required FontWeight weight}) =>
      TextStyle(
          color: color, fontFamily: font, fontSize: size, fontWeight: weight);
}
