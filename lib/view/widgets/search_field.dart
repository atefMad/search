import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:newwork/controller/translation.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../config/theme/app_colors.dart';

class SearchField extends StatefulWidget {
  SearchField({super.key});

  @override
  State<SearchField> createState() => _SearchFieldState();
}

class _SearchFieldState extends State<SearchField> {
  final trans = Get.put(TranslationService());

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      Container(
        padding: EdgeInsets.only(left: 30.w),
        height: 42.h,
        width: 510.w,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: colorFour,
        ),
        child: TextFormField(
          decoration: InputDecoration(
              border: InputBorder.none,
              suffixIcon: Padding(
                padding: EdgeInsets.only(right: 15.w),
                child: Image.asset(
                  'assets/images/Asset 10@2x.png',
                ),
              ),
              suffixIconConstraints:
                  BoxConstraints(maxHeight: 32.h, maxWidth: 42.w),
              prefixIcon: const Icon(
                Icons.close,
                color: colorThree,
              )),
        ),
      ),
      Container(
        height: 42.h,
        width: 42.w,
        alignment: Alignment.center,
        decoration: BoxDecoration(
            borderRadius: trans.isArabic()
                ? BorderRadius.only(
                    topRight: Radius.circular(20.r),
                    bottomRight: Radius.circular(20.r))
                : BorderRadius.only(
                    topLeft: Radius.circular(20.r),
                    bottomLeft: Radius.circular(20.r)),
            color: colorFour,
            boxShadow: [
              BoxShadow(
                  offset: Offset(3.w, 0),
                  color: Colors.black.withOpacity(.3),
                  blurRadius: 4,
                  spreadRadius: 0.2)
            ]),
        child: Image.asset(
          'assets/images/Asset 11@2x.png',
          height: 30.h,
          width: 30.w,
        ),
      )
    ]);
  }
}
