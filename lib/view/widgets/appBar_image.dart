import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';


class AppBarImage extends StatelessWidget {
  final String url;
  final void Function() onTap;
  const AppBarImage({super.key, required this.url, required this.onTap});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        width: 50.w,
        alignment: Alignment.center,
        child: Image.asset(
          'assets/images/$url',
          height: 20.h,
          width: 30.w,
        ),
      ),
    );
  }
}