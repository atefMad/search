import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../config/theme/app_colors.dart';

class LocationText extends StatelessWidget {
  const LocationText({super.key, required this.unit, required this.distance});

  final String unit;
  final double distance;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 110.w,
      child: Column(
        children: [
          Text(
            'location'.tr,
            style: TextStyle(
                color: colorFive, fontFamily: 'SourceSanaPro', fontSize: 16.sp),
          ),
          RichText(
            text: TextSpan(children: <TextSpan>[
              TextSpan(
                  text: unit,
                  style: TextStyle(
                      color: colorSix,
                      fontFamily: 'SourceSanaPro',
                      fontWeight: FontWeight.w900,
                      fontSize: 14.sp)),
              TextSpan(
                  text: " $distance",
                  style: TextStyle(
                      color: colorSix,
                      fontFamily: 'SourceSanaPro',
                      fontWeight: FontWeight.w900,
                      fontSize: 9.sp))
            ]),
          )
        ],
      ),
    );
  }
}
