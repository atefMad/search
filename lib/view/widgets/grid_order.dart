import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:newwork/view/widgets/status_bar.dart';

import '../../config/theme/app_colors.dart';
import '../../controller/translation.dart';
import 'circle_brand.dart';

class GridOrder extends StatelessWidget {
   GridOrder({super.key});
  final trans = Get.put(TranslationService());

  @override
  Widget build(BuildContext context) {
    return GridView.count(
      padding: EdgeInsets.symmetric(horizontal: 12.w, vertical: 12.h),
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      crossAxisCount: 3, // put 2 element in line
      // mainAxisSpacing: 12.0.h, // add space between elements  -up down
      // crossAxisSpacing: 12.w, // -left right
      childAspectRatio: 180.w / 233.h, // width / height
      children: List.generate(9, (index) {
        return Column(
          children: [
            const CircleBrand(
              distance: 4.5,
              unit: 'KM',
            ),
            StatusBar(
              color: Colors.green[400]!,
              height: 4.h,
              width: 30.w,
            ),
            Text(
              'resturant1'.tr,
              style: TextStyle(
                  color: colorFive,
                  fontFamily:  trans.isArabic()?  'Almarai' :  'SourceSansPro',
                  fontWeight: FontWeight.bold,
                  fontSize: 26.sp),
            )
          ],
        );
      }),
    );
  }
}
