import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:newwork/config/theme/app_colors.dart';
import 'package:newwork/controller/show_list.dart';
import 'package:newwork/view/widgets/appBar_image.dart';
import 'package:newwork/view/widgets/search_field.dart';

class AppBarWidget extends StatefulWidget {
  const AppBarWidget({super.key});

  @override
  State<AppBarWidget> createState() => _AppBarWidgetState();
}

class _AppBarWidgetState extends State<AppBarWidget> {
  bool gridView = true, gps = true, filter = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      // height: 110.h,
      margin: EdgeInsets.only(top: 20.h),
      width: 510.w,
      color: colorThree,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
           SearchField(),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            physics: const BouncingScrollPhysics(),
            child: Row(
              children: [
                TextButton(
                  onPressed: () {},
                  child: Container(
                    decoration: const BoxDecoration(
                        border: Border(
                            bottom: BorderSide(color: colorSix, width: 2))),
                    child: Text(
                      'Subscribers',
                      style: TextStyle(
                          color: colorSix,
                          fontFamily: 'SourceSansPro',
                          fontWeight: FontWeight.w800,
                          fontSize: 16.sp),
                    ),
                  ),
                ),
                Container(
                  width: 205.w,
                  height: 30.h,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.centerLeft,
                      end: Alignment.centerRight,
                      colors: <Color>[
                        colorThree.withOpacity(0.9),
                        colorTwo,
                      ],
                    ),
                  ),
                  child: GetBuilder<ShowListController>(
                    init: ShowListController(),
                    builder: (controller) => Row(
                      children: [
                        customDivider(),
                        AppBarImage(
                          url: controller.isGrid
                              ? 'Asset 6@2x.png'
                              : 'Asset 2@2x.png',
                          onTap: () {
                            controller.changeShow(true);
                          },
                        ),
                        customDivider(),
                        AppBarImage(
                          url: controller.isGrid
                              ? 'Asset 5@2x.png'
                              : 'Asset 9@2x.png',
                          onTap: () {
                            controller.changeShow(false);
                          },
                        ),
                        customDivider(),
                        AppBarImage(
                          url: gps ? 'Asset 7@2x.png' : 'Asset 3@2x.png',
                          onTap: () {
                            setState(() {
                              gps = !gps;
                            });
                          },
                        ), //7
                        customDivider(),
                        AppBarImage(
                          url: filter ? 'Asset 8@2x.png' : 'Asset 4@2x.png',
                          onTap: () {
                            setState(() {
                              filter = !filter;
                            });
                          },
                        ),
                        customDivider(),
                      ],
                    ),
                  ),
                ),
                appBarText('Products'),
                appBarText('Related ads'),
              ],
            ),
          )
        ],
      ),
    );
  }
}

customDivider() => Container(
      width: 1.w,
      height: 30.h,
      color: colorThree,
    );

appBarText(String label) => Padding(
      padding: EdgeInsets.symmetric(horizontal: 10.w),
      child: Text(
        label,
        style: TextStyle(
            color: colorFour.withOpacity(.6),
            fontFamily: 'SourceSansPro',
            fontSize: 16.sp),
      ),
    );
